import { Component, OnInit } from '@angular/core';
import { navLinks, NavLink } from '../../interfaces/menu';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  navLinks: NavLink[] = navLinks;

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

}
