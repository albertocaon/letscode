import { MatMenuModule } from '@angular/material/menu';
import { MenuComponent } from './menu.component';
import { MatButtonModule } from '@angular/material/button';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatMenuModule,
        MatButtonModule,
      ],
      declarations: [ 
        MenuComponent,
      ],
      providers: [
        { 
          provide: Router,
          useValue: routerSpy
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): number {
                  return 6;
                }
              }
            }
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render smMenu', () => {
    const fixture = TestBed.createComponent(MenuComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('#smMenu')).toBeTruthy();
  });

  it('menu should render trigger', () => {
    const fixture = TestBed.createComponent(MenuComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('#smMenuTrigger')).toBeTruthy();
  });

  it('navLinks should have proper values', () => {
    const mockedNavLinks = [
      // {
      //   path: 'home',
      //   label: 'HOME',
      //   icon: 'home'
      // },
      {
        path: 'meteo',
        label: 'METEO',
        icon: 'wb_sunny'
      },
      {
        path: 'info',
        label: 'INFO',
        icon: 'info'
      }
    ];
    expect(component.navLinks).toEqual(mockedNavLinks);
  });
});
