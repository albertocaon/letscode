import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-moon-icon',
  templateUrl: './moon-icon.component.html',
  styleUrls: ['./moon-icon.component.scss']
})
export class MoonIconComponent implements OnInit {

	@Input() moonPhase: number;
	moonPercent: string;

	constructor() { }

	ngOnInit(): void {
		this.moonPercent = (100 - (this.moonPhase * 200)) + '%';
	}

}
