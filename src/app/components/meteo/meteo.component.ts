import { Component, OnInit } from '@angular/core';
import { DarkskyService } from '../../services/darksky.service';
import { Geolocation, places, Place } from '../../interfaces/geolocation';
import { DarkSkyDataTree } from '../../interfaces/dark-sky-data-tree';
import { Forecast } from '../../interfaces/forecast';
import { DatePipe } from '@angular/common';
import { FormControl, Validators, AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-meteo',
  templateUrl: './meteo.component.html',
  styleUrls: ['./meteo.component.scss'],
  providers: [ DarkskyService ]
})
export class MeteoComponent implements OnInit {

	DSDataTree: DarkSkyDataTree;
	places: Place[] = places;
	selectedPlace: Place = places[0];
	custom: Place = places[1];
	loaded: boolean = false;
	today: Forecast[] = [];
	tomorrow: Forecast[] = [];
	dayAfterTomorrow: Forecast[] = [];


	locationForm = new FormGroup({
		latitude: new FormControl('', regexValidator(/([-+]?\d{1,2}([.]\d+)?)/)),
		longitude: new FormControl('', regexValidator(/[-+]?\d{1,3}([.]\d+)?/))
	});


	constructor(public _darkSky: DarkskyService, private datePipe: DatePipe) { }

	ngOnInit(): void {
		this.selectDefaultPlace();
	}

	selectDefaultPlace() {
		this.getData(this.places[0]);
	}

	selectCustomPlace() {
		if (this.customLocationIsValid()) {
			this.updateCustomLocation();
			this.selectedPlace = this.custom;
		}

		this.getData(this.selectedPlace);
	}

	getData(place: Place) {
		this.selectedPlace = place;
		this._darkSky.getData(this.selectedPlace.geolocation.lat, this.selectedPlace.geolocation.lon)
		.subscribe((data: DarkSkyDataTree) => {
			this.DSDataTree = data;
			const today = parseInt(this.datePipe.transform(Date.now(), 'd'));
			const dayAfter = parseInt(this.datePipe.transform((Date.now() + (1000 * 60 * 60 * 24)), 'd'));
			const afterDayAfter = parseInt(this.datePipe.transform((Date.now() + (2 * 1000 * 60 * 60 * 24)), 'd'));
			this.DSDataTree.hourly.data.forEach((v, k) => {
				const day = parseInt(this.datePipe.transform(v.time * 1000, 'd'));
				if (day === today) {
					this.today.push(v);
				} else if (day === dayAfter) {
					this.tomorrow.push(v);
				} else if (day === afterDayAfter) {
					this.dayAfterTomorrow.push(v);
				}
			});

			this.loaded = true;	
			setTimeout(() => {
				this._darkSky.loading = false;
				this.loaded = false;
			}, 600);
		});
	}

	customLocationIsValid() : boolean {
		return !this.locationForm.hasError('latitude') && !this.locationForm.hasError('longitude') &&
			this.locationForm.get('latitude').value !== '' && this.locationForm.get('longitude').value !== '';
	}

	updateCustomLocation(): void {
		this.custom.geolocation.lat = this.locationForm.get('latitude').value;
		this.custom.geolocation.lon = this.locationForm.get('longitude').value;
	}

}

export function regexValidator(regex: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const invalid = regex.test(control.value);
    return invalid ? {invalid: {value: control.value}} : null;
  };
}
