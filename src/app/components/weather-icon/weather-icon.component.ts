import { Component, OnInit, OnChanges, SimpleChanges, SimpleChange, Input } from '@angular/core';
import { iconCodes } from '../../interfaces/icons';

@Component({
  selector: 'app-weather-icon',
  templateUrl: './weather-icon.component.html',
  styleUrls: ['./weather-icon.component.scss']
})
export class WeatherIconComponent implements OnInit, OnChanges {

	@Input() icon: string;
	iconCode: string;
	constructor() { }

	ngOnChanges(changes: SimpleChanges) {
		const icon: SimpleChange = changes.icon;
		if (icon) {
			this.icon = icon.currentValue;
			this.iconCode = iconCodes[this.icon];
		}
	}

	ngOnInit(): void {
		this.iconCode = iconCodes[this.icon];
	}

}
