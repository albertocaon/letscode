import { Component, OnInit } from '@angular/core';
import { infoLinks, NavLink } from '../../interfaces/menu';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  name: string = 'Alberto Caon';
  position: string = 'Fullstack Developer';
  infoLinks: NavLink[] = infoLinks;

  constructor() { }

  ngOnInit(): void {
  }

}
