import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoComponent } from './info.component';

describe('InfoComponent', () => {
  let component: InfoComponent;
  let fixture: ComponentFixture<InfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have name Alberto Caon', () => {
    expect(component.name).toEqual('Alberto Caon');
  });

  it('should have position Dev', () => {
    expect(component.position).toEqual('Fullstack Developer');
  });

  it('should have infoItems', () => {
    const mockedInfoLinks = [
      {
        path: 'https://bitbucket.org/albertocaon/',
        label: 'BITBUCKET Repository',
        icon: 'integration_instructions'
      },
      {
        path: 'https://www.linkedin.com/in/alberto-caon-aaa47234/',
        label: 'LINKEDIN Profile',
        icon: 'portrait'
      },
      {
        path: 'https://prezzemolofresco.com/',
        label: 'PREZZEMOLO FRESCO Foodies WebApp',
        icon: 'restaurant_menu'
      },
      {
        path: 'https://apprediction.com/',
        label: 'APPREDICTION Magic WebApp',
        icon: 'brightness_auto'
      },
      {
        path: 'https://letscode.es/',
        label: 'LET\'S CODE Meteo WebApp',
        icon: 'keyboard_arrow_right'
      }
    ];
    expect(component.infoLinks).toEqual(mockedInfoLinks);
  });
});
