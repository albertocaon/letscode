export const navLinks: NavLink[] = [
	// {
	// 	path: 'home',
	// 	label: 'HOME',
	// 	icon: 'home'
	// },
	{
		path: 'meteo',
		label: 'METEO',
		icon: 'wb_sunny'
	},
	{
		path: 'info',
		label: 'INFO',
		icon: 'info'
	}
]

export const infoLinks: NavLink[] = [
	{
		path: 'https://bitbucket.org/albertocaon/',
		label: 'BITBUCKET Repository',
		icon: 'integration_instructions'
	},
	{
		path: 'https://www.linkedin.com/in/alberto-caon-aaa47234/',
		label: 'LINKEDIN Profile',
		icon: 'portrait'
	},
	{
		path: 'https://prezzemolofresco.com/',
		label: 'PREZZEMOLO FRESCO Foodies WebApp',
		icon: 'restaurant_menu'
	},
	{
		path: 'https://apprediction.com/',
		label: 'APPREDICTION Magic WebApp',
		icon: 'brightness_auto'
	},
	{
		path: 'https://letscode.es/',
		label: 'LET\'S CODE Meteo WebApp',
		icon: 'keyboard_arrow_right'
	}
]

export interface NavLink {
	"path": string;
	"label": string;
	"icon": string;
}