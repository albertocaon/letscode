export const iconCodes = {
	'clear-day': 'wb_sunny',
	'clear-night': 'star_outline',
	'rain': 'beach_access',
	'snow': 'ac_unit',
	'sleet': 'insights',
	'wind': 'gesture',
	'fog': 'texture',
	'cloudy': 'cloud',
	'partly-cloudy-day': 'cloud_circle',
	'partly-cloudy-night': 'nights_stay',
	'hail': 'grain',
	'thunderstorm': 'flash_on',
	'tornado': 'toys'
}