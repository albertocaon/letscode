export class Geolocation {
	lat: number;
	lon: number;
}

export interface Place {
	name: string;
	geolocation: Geolocation;
}

const joanic = {
	name: 'JOANIC', geolocation: { lat: 41.406029, lon: 2.162955 }
}
const custom = {
	name: 'CUSTOM', geolocation: { lat: 42.293923, lon: 2.139807 }
}
const tor = {
	name: 'TOR' , geolocation: { lat: 42.095496, lon: 3.066937 }
}
const resana = {
	name: 'RESANA', geolocation: { lat: 45.638823, lon: 11.966471 }
}
const tenryuzanji = {
	name: 'TENRYUZANJI', geolocation: { lat: 46.056442, lon: 11.617603 }
}
const caorle = {
	name: 'CAORLE', geolocation: { lat: 45.596884, lon: 12.877267}
}
const cadoneghe = {
	name: 'CADONEGHE', geolocation: { lat: 45.444207, lon: 11.918362 }
}
const rosolinaMare = {
	name: 'ROSOLINA MARE', geolocation: { lat: 45.130701, lon: 12.325567 }
}
const cismonDelGrappa = {
	name: 'CISMON DEL GRAPPA', geolocation: { lat: 45.921370, lon: 11.727568 }
}
const osseja = {
	name: 'OSSEJA', geolocation: { lat: 42.395104, lon: 1.991442 }
}

const formentera = {
	name: 'FORMENTERA', geolocation: { lat: 38.682775, lon: 1.492004 }
}
const tamariu = {
	name: 'TAMARIU', geolocation: { lat: 41.916791, lon: 3.205975 }
}

export const places: Place[] = [joanic, custom];