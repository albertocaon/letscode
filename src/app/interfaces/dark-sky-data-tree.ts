import { Forecast } from './forecast';
import { DailyForecast } from './daily-forecast';
export class DarkSkyDataTree {
	currently: Forecast;
	daily: {
		data: DailyForecast[];
		icon: string;
		summary: string;
	};
	flags: {
		units: string;
	};
	hourly: {
		data: Forecast[];
		icon: string;
		summary: string;	
	};
	latitude: number;
	longitude: number;
	offset: number;
	timezone: string;
}