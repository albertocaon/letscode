import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { InfoComponent } from './components/info/info.component';
import { MeteoComponent } from './components/meteo/meteo.component';

const routes: Routes = [
	// { 
	//     path: 'home', 
	//     component: HomeComponent, data: { animation: 'HomePage' } 
	// },
	{ 
	    path: 'meteo', 
	    component: MeteoComponent
	},
	{ 
	    path: 'info', 
	    component: InfoComponent
	},
	{ path: '', redirectTo: 'meteo', pathMatch: 'full'},
	{
		path: '**', redirectTo: '404'
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
