import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { DarkskyService } from './darksky.service';
import { fakeAsync, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

describe('DarkskyService', () => {
	let injector: TestBed;
	let service: DarkskyService;
	let httpMock: HttpTestingController;

	beforeEach(() => {
	    TestBed.configureTestingModule({
	      imports: [HttpClientTestingModule],
	      providers: [
	        DarkskyService
	      ]
	    });
	    injector = getTestBed();
	    httpMock = injector.get(HttpTestingController);
	    service = injector.get(DarkskyService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});
