import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DarkSkyDataTree } from '../interfaces/dark-sky-data-tree';
import { Observable } from 'rxjs';

@Injectable()
export class DarkskyService {
	//private darkskySecretKey = '62c8ff4ff05f80c632664230ecd61e53'; obsolete from 2018/06/19
	private darkskySecretKey = 'b0ae2f028f93afa611991b204d301d21';
	private darkskyAPIURL = 'https://cors-anywhere.herokuapp.com/https://api.darksky.net';
	
	private units = 'si';
	private APIURL = '';
	private data;
	public loading: boolean = false;

  	constructor(private http: HttpClient) { }

  	getData(lat, lon): Observable<any> {
  		this.loading = true;
  		this.APIURL = this.darkskyAPIURL + '/forecast/' 
			+ this.darkskySecretKey + '/' 
			+ lat +',' + lon 
			+ '?units=' + this.units;

		// this.APIURL = '/forecast/b0ae2f028f93afa611991b204d301d21/37.8267,-122.4233';

		return this.http.get<any>(this.APIURL);
  	}
}
