import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wind'
})
export class WindPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
  	let windName = 'unknown';
  	if ((value > 348.75 && value <= 360) || (value >=0 && value <= 11.25)) {
  		windName = 'Tramuntana (N)';
  	}
  	if (value > 11.25 && value <= 33.75) {
  		windName = 'Tramuntana-Gregal (N-NE)';
  	}
  	if (value > 33.75 && value <= 56.25) {
  		windName = 'Gregal (NE)';
  	}
  	if (value > 56.25 && value <= 78.75) {
  		windName = 'Gregal-Llevant (NE-E)';
  	}
  	if (value > 78.75 && value <= 101.25) {
  		windName = 'Llevant (E)';
  	}
  	if (value > 101.25 && value <= 123.75) {
  		windName = 'Llevant-Xaloc (E-SE)';
  	}
  	if (value > 123.75 && value <= 146.25) {
  		windName = 'Xaloc (SE)';
  	}
  	if (value > 146.25 && value <= 168.75) {
  		windName = 'Xaloc-Migjorn (SE-S)';
  	}
  	if (value > 168.75 && value <= 191.25) {
  		windName = 'Migjorn (S)';
  	}
  	if (value > 191.25 && value <= 213.75) {
  		windName = 'Migjorn-Garbí (S-SW)';
  	}
  	if (value > 213.75 && value <= 236.25) {
  		windName = 'Garbí (SW)';
  	}
  	if (value > 236.25 && value <= 258.75) {
  		windName = 'Garbí-Ponent (SW-W)';
  	}
  	if (value > 258.75 && value <= 281.25) {
  		windName = 'Ponent (W)';
  	}
  	if (value > 281.25 && value <= 303.75) {
  		windName = 'Ponent-Mestral (W-NW)';
  	}
  	if (value > 303.75 && value <= 326.25) {
  		windName = 'Mestral (NW)';
  	}
  	if (value > 326.25 && value <= 348.75) {
  		windName = 'Mestral-Tramuntana (NW-N)';
  	}
	return windName;
  }

}
